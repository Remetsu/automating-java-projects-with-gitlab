package org.hsmith.medium;

final class WorkerClass {
    int sum(final int value1, final int value2) {
        return value1 + value2;
    }
}
